package model;




import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Tranzaction {
	 @Id
     private int id;
     private String description;
     private int quantity;
     private double price;
     private Date date;
     private int idBroker;
     private int idInstrument;
     private String type;
	
     public Tranzaction(int id,  int quantity,double price, Date date, int idBroker, int idInstrument,String type){
         this.setId(id);
    	 this.setQuantity(quantity);
    	 this.setPrice(price);
    	 this.setDate(date);
    	 this.setIdBroker(idBroker);
    	 this.setIdInstrument(idInstrument);
    	 this.setType(type);
     }
     
     public Tranzaction( int quantity,double price, Date date, int idBroker, int idInstrument,String type){
         
    	 this.setQuantity(quantity);
    	 this.setPrice(price);
    	 this.setDate(date);
    	 this.setIdBroker(idBroker);
    	 this.setIdInstrument(idInstrument);
    	 this.setType(type);
     }
     
     




	public int getId() {
		return id;
	   
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getIdBroker() {
		return idBroker;
	}

	public void setIdBroker(int idBroker) {
		this.idBroker = idBroker;
	}

	public int getIdInstrument() {
		return idInstrument;
	}

	public void setIdInstrument(int idInstrument) {
		this.idInstrument = idInstrument;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}
     
	
}
