package model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Broker {
	@Id
	private int idbroker;
	private String name;
	private String username;
	private String password;
	private double fund;
	
	
	public Broker(String name, String username, String password, double fund, int id){
		this.setName(name);
		this.setUsername(username);
		this.setPassword(password);
		this.setFund(fund);
		this.setId(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getFund() {
		return fund;
	}

	public void setFund(double fund) {
		this.fund = fund;
	}

	public int getId() {
		return idbroker;
	}

	public void setId(int idbroker) {
		this.idbroker = idbroker;
	}

}
