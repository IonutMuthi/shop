package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.TranzactionDao;
import dto.TranzactionInterface;
import model.Broker;
import model.Instrument;
import model.Tranzaction;

public class TranzactionGui extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private TranzactionInterface tdao=new TranzactionDao();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TranzactionGui frame = new TranzactionGui(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TranzactionGui(Broker broker) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		try {
			table = new JTable(updateTable(broker));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		table.setBounds(5, 0, 424, 200);
		contentPane.add(table);
		
	}
public DefaultTableModel updateTable(Broker broker) throws SQLException{
		
		
		String[] cols={"Tranzaction id","Quantity","Price","Date","Type","idInstrument"};
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Intrument id");
		model.addColumn("Name");
		model.addColumn("Price");
		model.addRow(cols);
		
			for(Tranzaction t:tdao.getTransactionsByBroker(broker)){
				
				Object[] o={t.getId(),t.getQuantity(),t.getPrice(),t.getDate(),t.getType(),t.getIdInstrument()};
				model.addRow(o);
			
			
			}
			
	return model;

	
	}

}
