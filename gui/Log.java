package gui;


import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.BrokerDao;
import dto.BrokerInterface;
import model.Broker;




public class Log extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JLabel lblPassword;
	private JLabel lblUserName;
	private String user,password;

	private BrokerInterface bi=new BrokerDao();
	private Broker broker;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Log frame = new Log();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Log() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel lblUsernameOrPassword = new JLabel("");
		lblUsernameOrPassword.setVisible(false);
		lblUsernameOrPassword.setForeground(Color.RED);
		lblUsernameOrPassword.setBounds(77, -12, 207, 48);
		contentPane.add(lblUsernameOrPassword);
		
		JButton btnAutentification = new JButton("Autentification");
		btnAutentification.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
	          try {
	        	 user=txtUsername.getText();
	        	 password=txtPassword.getText();
	        	
	        	  broker=bi.findByUsername(user);
	        	    if(password.equals(broker.getPassword())){
	        	    	Loged log=new Loged(broker);
	        	    	log.setVisible(true);
	        	    	 System.out.println("loged");
	        	    }
	        	  
	          }catch (Exception e1) {
					e1.printStackTrace();
					
				}
	          
			}
			
		});
		
		lblUserName = new JLabel("User name:");
		lblUserName.setBounds(5, 40, 80, 14);
		contentPane.add(lblUserName);
		
		btnAutentification.setBounds(40, 120, 200, 23);
		contentPane.add(btnAutentification);
		
		

		
		txtUsername = new JTextField();
		txtUsername.setBounds(110, 30, 110, 30);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		txtPassword = new JTextField();
		txtPassword.setFont(new Font("Symbol", Font.PLAIN, 11));
		txtPassword.setBackground(new Color(255, 255, 255));
		txtPassword.setBounds(110, 70, 110, 30);
		contentPane.add(txtPassword);
		txtPassword.setColumns(10);
		
		lblPassword = new JLabel("Password:");
		lblPassword.setBounds(5, 80, 80, 14);
		contentPane.add(lblPassword);
		
		
		
		
	}
}
