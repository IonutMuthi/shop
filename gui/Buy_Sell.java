package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.synth.SynthOptionPaneUI;

import dto.BrokerDao;
import dto.BrokerInterface;
import dto.InstrumentDao;
import dto.InstrumentInterface;
import dto.TranzactionDao;
import dto.TranzactionInterface;
import model.Broker;
import model.Instrument;
import model.Tranzaction;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;

public class Buy_Sell extends JFrame {

	private JPanel contentPane;
	private JTextField txtIid;
	private JTextField textField;
	private BrokerInterface bdao=new BrokerDao();
    private InstrumentInterface idao=new InstrumentDao();
    private TranzactionInterface tdao=new TranzactionDao();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					int f=0;
					Buy_Sell frame = new Buy_Sell(f);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Buy_Sell(int idBroker) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblInstrumentId = new JLabel("Instrument id");
		lblInstrumentId.setBounds(10, 11, 96, 14);
		contentPane.add(lblInstrumentId);
		
		JLabel lblQuantity = new JLabel("quantity");
		lblQuantity.setBounds(10, 47, 96, 14);
		contentPane.add(lblQuantity);
		
		txtIid = new JTextField();
		txtIid.setBounds(131, 8, 86, 20);
		contentPane.add(txtIid);
		txtIid.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(131, 44, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblOp = new JLabel("");
		lblOp.setBounds(131, 138, 170, 14);
		lblOp.setVisible(false);
		contentPane.add(lblOp);
		
		JButton btnBuy = new JButton("buy");
		btnBuy.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int idIntsrument=Integer.parseInt(txtIid.getText());
				int quantity= Integer.parseInt(textField.getText());
				Instrument instr=idao.findById(idIntsrument);
				Broker broker=bdao.findById(idBroker);
				Date today = new Date(System.currentTimeMillis());
				int dd = today.getDate();
				int mm = today.getMonth()+1; //January is 0!
				int yyyy = today.getYear()+1900; // time is only mesured form 1900 to curent date so we need to add 1900 to ghet the correct year
					
				String date=""+dd+"/"+mm+"/"+yyyy;		
				tdao.insert(broker, instr, quantity, "buy");
				bdao.updateFund(broker,quantity);
			    lblOp.setText("Instrument bought");
			    lblOp.setVisible(true);
			}
		});
		btnBuy.setBounds(279, 11, 89, 23);
		contentPane.add(btnBuy);
		
		JButton btnSell = new JButton("sell");
		btnSell.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int idIntsrument=Integer.parseInt(txtIid.getText());
				int quantity= Integer.parseInt(textField.getText());
				Instrument instr=idao.findById(idIntsrument);
				Broker broker=bdao.findById(idBroker);
				Date today = new Date(System.currentTimeMillis());
				int dd = today.getDate();
				int mm = today.getMonth()+1; //January is 0!
				int yyyy = today.getYear()+1900; // time is only mesured form 1900 to curent date so we need to add 1900 to ghet the correct year
					
				String date=""+dd+"/"+mm+"/"+yyyy;		
				tdao.insert(broker, instr, quantity, "sell");
				bdao.updateFund(broker,  quantity);
				  lblOp.setText("Instrument sold");
				    lblOp.setVisible(true);
			}
		});
		btnSell.setBounds(279, 43, 89, 23);
		contentPane.add(btnSell);
		
	
	}
}
