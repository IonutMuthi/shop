package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import dto.InstrumentDao;
import dto.InstrumentInterface;
import model.Instrument;

public class InstrumentGui extends JFrame {

	private JPanel contentPane;
	private InstrumentInterface idao=new InstrumentDao();
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InstrumentGui frame = new InstrumentGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InstrumentGui() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		try {
			table = new JTable(updateTable());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		table.setBounds(5, 0, 424, 200);
		contentPane.add(table);
	}
	
	
public DefaultTableModel updateTable() throws SQLException{
		
		
		String[] cols={"Intrument id","Name","Price"};
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Intrument id");
		model.addColumn("Name");
		model.addColumn("Price");
		model.addRow(cols);
			for(Instrument i:idao.findAll()){
				
				
				Object[] o={i.getId(),i.getName(),i.getPrice()};
				model.addRow(o);
			
			
			}
			
	return model;

	
	}

}
