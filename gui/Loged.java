package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Broker;

import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

public class Loged extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Loged frame = new Loged(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param broker 
	 */
	public Loged(Broker broker) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 180, 280);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnInstruments = new JButton("Instruments");
		btnInstruments.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				InstrumentGui iGui=new InstrumentGui();
				iGui.setVisible(true);
			}
		});
		btnInstruments.setBounds(10, 11, 125, 23);
		contentPane.add(btnInstruments);
		
		JButton btnTrades = new JButton("Trades");
		btnTrades.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				TranzactionGui tranzactions=new TranzactionGui(broker);
				tranzactions.setVisible(true);
			}
		});
		btnTrades.setBounds(10, 60, 125, 23);
		contentPane.add(btnTrades);
		
		JButton btnBuysell = new JButton("Buy/Sell");
		btnBuysell.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Buy_Sell bs=new Buy_Sell(broker.getId());
				bs.setVisible(true);
			}
		});
		btnBuysell.setBounds(10, 100, 125, 23);
		contentPane.add(btnBuysell);
		
		JButton btnEndOfDay = new JButton("End of day");
		btnEndOfDay.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				btnBuysell.setEnabled(false);
				Report report =new Report(broker);
				report.setVisible(true);
				
			}
		});
		btnEndOfDay.setBounds(10, 150, 125, 23);
		contentPane.add(btnEndOfDay);
		
		JLabel lblCurrentFund = new JLabel("Current fund: "+broker.getFund());
		lblCurrentFund.setBounds(10, 200, 144, 14);
		contentPane.add(lblCurrentFund);
	}
}
