package unitTest;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.Session;
import org.junit.Test;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import connection.ConnectionFactory;
import dto.BrokerDao;
import dto.BrokerDto;
import dto.BrokerInterface;
import dto.InstrumentDao;
import dto.InstrumentDto;
import dto.InstrumentInterface;
import dto.TranzactionDao;
import dto.TranzactionDto;
import dto.TranzactionInterface;
import model.Broker;
import model.Instrument;
import model.Tranzaction;

public class crudTest {
	
	
	@Test 
	public void findByIDH(){
		TranzactionInterface TranzactionDTO = new TranzactionDto();
        Tranzaction lastTranzaction = getLastTranzactionH();
        Tranzaction Tranzaction = TranzactionDTO.findById(lastTranzaction.getId());
        System.out.println(Tranzaction.toString());
        assertEquals(lastTranzaction.getId(),Tranzaction.getId());	
	}
	
	@Test
	void insertH() {
		TranzactionInterface TranzactionDTO = new TranzactionDto();
	    Tranzaction lastTranzaction = getLastTranzactionH();
	    BrokerInterface b = new BrokerDto();
	    InstrumentInterface i = new InstrumentDto();

        String hql = "SELECT COUNT(Tranzaction.id) FROM Tranzaction Tranzaction";
        Session session = (Session) ConnectionFactory.getConnection("Hibenate");
        Long count = (Long) session.createQuery(hql).list().get(0);
        session.close();

        try { 
        	 Broker br=b.findById(lastTranzaction.getIdBroker());
        	 Instrument ist=i.findById(lastTranzaction.getIdInstrument());
        	  TranzactionDTO.insert(br,ist,lastTranzaction.getQuantity(),lastTranzaction.getType());
               session = (Session) ConnectionFactory.getConnection("Hibenate");
            Long updatedCount = (Long) session.createQuery(hql).list().get(0);
            session.close();
            updatedCount--;
            assertEquals(count,updatedCount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void updateH() {
    	TranzactionInterface TranzactionDTO = new TranzactionDto();
          Tranzaction lastTranzaction = getLastTranzactionH();
        Tranzaction updatedTranzaction = getLastTranzactionH();

        int newQuantity = lastTranzaction.getQuantity()+1;
        updatedTranzaction.setQuantity(newQuantity);
        TranzactionDTO.update(updatedTranzaction);

        lastTranzaction = getLastTranzactionH();
        assertEquals(updatedTranzaction.toString(),lastTranzaction.toString());
    }

    @Test
    void deleteH() {
        TranzactionInterface TranzactionDTO = new TranzactionDto();
        String hql = "SELECT COUNT(Tranzaction.id) FROM Tranzaction Tranzaction";
        Session session = (Session) ConnectionFactory.getConnection("Hibenate");
        Long count = (Long) session.createQuery(hql).list().get(0);
        session.close();

        TranzactionDTO.delete(getLastTranzactionH());
        session = (Session) ConnectionFactory.getConnection("Hibenate");
        Long updatedCount = (Long) session.createQuery(hql).list().get(0);
        session.close();
        updatedCount++;
        assertEquals(count,updatedCount);

    }
	
	public Tranzaction getLastTranzactionH(){
		 Session session=(Session) ConnectionFactory.getConnection("Hibernate");;
	
		 String hql = "SELECT max(Tranzaction.id) FROM Tranzaction Tranzaction";
	        String selectTransationHQL = "FROM Tranzaction t WHERE t.id =:maxId";
	        Integer maxId = (Integer) session.createQuery(hql).list().get(0);
	        Tranzaction Tranzaction = (Tranzaction) session.createQuery(selectTransationHQL).setParameter("maxId",maxId).list().get(0);
	        session.evict(Tranzaction);
	        session.close();
	        return Tranzaction;
	}
	
	
    @Test
    void findById() {
        TranzactionInterface TranzactionDao = new TranzactionDao();

        int validTranzactionID = -1;

        try {
            Tranzaction returnedTranzaction = getLastTranzaction();
            Tranzaction Tranzaction = TranzactionDao.findById(returnedTranzaction.getId());
            assertEquals(Tranzaction, returnedTranzaction);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    @Test
    void insert() {
        try {
            TranzactionInterface TranzactionDao = new TranzactionDao();
            Tranzaction Tranzaction = getLastTranzaction();
            BrokerInterface b = new BrokerDao();
    	    InstrumentInterface i = new InstrumentDao();

    	   Broker br=b.findById(Tranzaction.getIdBroker());
       	 Instrument ist=i.findById(Tranzaction.getIdInstrument());
       	
         TranzactionDao.insert(br,ist,Tranzaction.getQuantity(),Tranzaction.getType());
          Tranzaction insertedTranzaction = getLastTranzaction();
            Tranzaction.setId(insertedTranzaction.getId());
            assertEquals(insertedTranzaction,Tranzaction);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    we get the last Tranzaction and try to update it;
     */
   @Test
    void update() {
        Connection connection = (Connection) ConnectionFactory.getConnection("jdbcconnection");
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Tranzaction Tranzaction = getLastTranzaction();
            Tranzaction.setQuantity(Tranzaction.getQuantity()+1);
            TranzactionInterface TranzactionDao = new TranzactionDao();
            TranzactionDao.update(Tranzaction);

            Tranzaction newTranzaction = getLastTranzaction();
            assertEquals(newTranzaction,Tranzaction);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

    /*try to delete last Tranzaction*/
    @Test
    void delete() {
        TranzactionInterface TranzactionDao = new TranzactionDao();
        Connection connection = (Connection) ConnectionFactory.getConnection("jdbcconnection");
        Statement statement = null;
        ResultSet resultSet = null;
        int before=0,after=0;
        try{
            String sql = "SELECT COUNT(id) FROM Tranzaction";
            statement = (Statement) connection.createStatement();
            resultSet = statement.executeQuery(sql);
            if(resultSet.next()){
                before = resultSet.getInt(1);
            }
            Tranzaction Tranzaction = getLastTranzaction();
            TranzactionDao.delete(Tranzaction);

            statement = (Statement) connection.createStatement();
            resultSet = statement.executeQuery(sql);
            if(resultSet.next()){
                after = resultSet.getInt(1);
            }

            assertEquals(before,after+1);

        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

    Tranzaction getLastTranzaction() throws SQLException {
        Connection connection = (Connection) ConnectionFactory.getConnection("jdbcconnection");
        Statement statement = null;
        ResultSet resultSet = null;
        Tranzaction returnedTranzaction = null;

        InstrumentInterface instrumentDAO = new InstrumentDao();
        BrokerInterface brokerDAO = new BrokerDao();
        String sql = "SELECT *  FROM Tranzaction where id = (SELECT MAX(id) FROM Tranzaction)";
        statement = (Statement) connection.createStatement();
        resultSet = statement.executeQuery(sql);
        if(resultSet.next()) {

            Broker broker = brokerDAO.findById(resultSet.getInt("broker_id"));
            Instrument instrument = instrumentDAO.findById(resultSet.getInt("instrument_id"));
            returnedTranzaction = new Tranzaction(
            		 resultSet.getInt("quantity"),
                     resultSet.getDouble("price"),
                     resultSet.getDate("date"),             
                     broker.getId(),
                     instrument.getId(),
                     resultSet.getString("type")
            );
        }
            return returnedTranzaction;

    }

}
