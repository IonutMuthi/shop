package connection;

import java.sql.Connection;

import org.hibernate.Session;

public class ConnectionFactory {
	 private static Connection connection;
	    private static Session session;

	    public static Object getConnection(String connectionType){
	        if(connectionType == null){
	            connection = null;
	        }
	        if(connectionType.equalsIgnoreCase("jdbc")){
	            connection = JDBCConnection.getConnection();
	        }
	        else if(connectionType.equalsIgnoreCase("Hibernate")){
	            session = HibernateConnection.getSession();
	            return session;
	        }
	        return connection;
	    }
}
