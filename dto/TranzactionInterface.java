package dto;


import java.util.ArrayList;

import model.Broker;
import model.Instrument;
import model.Tranzaction;

public interface TranzactionInterface {
	 ArrayList<Tranzaction> getTransactionsByBroker(Broker broker);
	 Tranzaction findById(int id);
    void update(Tranzaction transaction);
	    void delete(Tranzaction transaction);
	    ArrayList<Tranzaction> getTodaysTransactions(String date, Broker broker);
	    ArrayList<Tranzaction> getIntervalTransactions(String date1, String date2, Broker broker);
	    double getFundBeforeDate(String date, Broker broker);
		Tranzaction insert(Broker broker, Instrument instrument, Integer quantity, String type);

}
