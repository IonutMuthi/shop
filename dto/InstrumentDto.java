package dto;

import java.util.ArrayList;

import org.hibernate.Session;

import connection.ConnectionFactory;
import model.Instrument;

public class InstrumentDto implements InstrumentInterface {

	 private Session session;

	    public InstrumentDto(){
	        this.session = (Session) ConnectionFactory.getConnection("Hibernate");
	    }

	    @Override
	    public Instrument findById(int id) {
	        Instrument instrument = session.get(Instrument.class,id);
	        return instrument;
	    }

	    @Override
	    public ArrayList<Instrument> findAll() {
	    	ArrayList<Instrument> result = (ArrayList<Instrument>) session.createQuery("from Instrument").list();
	        return result;
	    }

	    @Override
	    public Instrument findByName(String name) {
	        String hql = "SELECT instrument FROM Instrument instrument WHERE instrument.name =:name";
	        return (Instrument) session.createQuery(hql).setParameter("name",name).uniqueResult();
	    }

}
