package dto;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.stream.Collectors;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import connection.ConnectionFactory;
import model.Broker;

import model.Instrument;
import model.Tranzaction;

public class TranzactionDao implements TranzactionInterface {

    private static final String findByBrokerStatement = "SELECT *, FROM transaction where broker_idbroker = ?";
    private static final String insertStatement = "INSERT INTO transaction (`quantity`, `price`, `date`, `broker_idbroker`, `instrument_idinstrument`, `type`) VALUES (?, ?, ?,?, ?,?);";
    private static final String findByIdStatement = "SELECT * FROM transaction WHERE transaction.idtransaction = ?";
    private static final String updateTranzactionString = "UPDATE transaction SET price = ?, quantity = ? WHERE id = ?";
    private static final String deleteTranzactionString = "DELETE FROM transaction WHERE id = ?";
    private static final String selectBeforeString = "SELECT *,Tranzaction.id AS TranzactionID FROM Tranzaction WHERE date < ? AND broker_id = ?";
    private static final String selectBetweenString = "SELECT *,Tranzaction.id AS TranzactionID FROM Tranzaction WHERE date >= ? AND date <= ? AND broker_id = ?";


    private Connection connection;
    private ResultSet resultSet = null;
    private PreparedStatement statement= null;
    private BrokerDao brokerDAO;
    private InstrumentDao InstrumentDao;

    public TranzactionDao(){
        connection = (Connection) ConnectionFactory.getConnection("jdbc");
        brokerDAO = new BrokerDao();
        InstrumentDao = new InstrumentDao();
    }

  
    public Tranzaction findById(int id) {
        Tranzaction result = null;
        try{
            statement = (PreparedStatement) connection.prepareStatement(findByIdStatement);
            statement.setInt(1,id);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                Broker broker = brokerDAO.findById(resultSet.getInt("broker_idbroker"));
                Instrument instrument = InstrumentDao.findById(resultSet.getInt("instrument_idinstrument"));

                result = new Tranzaction(
                		   resultSet.getInt("quantity"),
                           resultSet.getDouble("price"),
                           resultSet.getDate("date"),             
                           broker.getId(),
                           instrument.getId(),
                           resultSet.getString("type")
                );
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }
    
    @Override
    public Tranzaction insert(Broker broker,Instrument instrument, Integer quantity, String type) {
        int insertedId = -1;
        Tranzaction Tranzaction= null;
        double amount = instrument.getPrice()*quantity;
        double newFund;
        if(type.equalsIgnoreCase("buy")){
            newFund = broker.getFund()-amount;
        }
        else
            newFund = broker.getFund()+amount;
        if(newFund<0){
          System.out.println("Insufficient funds!");
        }
        /*
        get today's date - the date in which the Tranzaction is made
         */
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date currentDate = Calendar.getInstance().getTime();

        Date date = new Date(currentDate.getTime());
        System.out.println(date);
        System.out.println("here");
        try{
        	
            statement = (PreparedStatement) connection.prepareStatement(insertStatement,Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1,quantity);
            statement.setDouble(2,amount);
            statement.setString(3,dateFormat.format(date));
            statement.setInt(4,broker.getId());
            statement.setInt(5,instrument.getId());
            statement.setString(6,type);
            statement.executeUpdate();

            resultSet = statement.getGeneratedKeys();
            if(resultSet.next()){
                insertedId = resultSet.getInt(1);
                Tranzaction = findById(insertedId);
                if(type.equalsIgnoreCase("buy"))
                brokerDAO.updateFund(broker,amount);
                else
                    brokerDAO.updateFund(broker,-amount);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return Tranzaction;
    }

    @Override
    public void update(Tranzaction Tranzaction) {
        try {
            statement = (PreparedStatement) connection.prepareStatement(updateTranzactionString);
            statement.setDouble(1, Tranzaction.getPrice());
            statement.setInt(2, Tranzaction.getQuantity());
            statement.setInt(3,Tranzaction.getId());
            statement.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Tranzaction Tranzaction) {
        try{
            statement = (PreparedStatement) connection.prepareStatement(deleteTranzactionString);
            statement.setInt(1,Tranzaction.getId());
            statement.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }



    @Override
    public double getFundBeforeDate(String date, Broker broker){
        double initialFund = 1000000.00;
        ArrayList<Tranzaction> before = new ArrayList<>();
        try {
            statement = (PreparedStatement) connection.prepareStatement(selectBeforeString, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, date);
            statement.setInt(2, broker.getId());
            resultSet = statement.executeQuery();
            before = createTranzactionArrayList(resultSet, broker);
            statement.close();

            for(Tranzaction t : before){
                if(t.getType().equalsIgnoreCase("buy")){
                    initialFund = initialFund-t.getPrice();
                }
                if(t.getType().equalsIgnoreCase("sell")){
                    initialFund = initialFund+t.getPrice();
                }
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return initialFund;
    }


    public ArrayList<Tranzaction> createTranzactionArrayList(ResultSet resultSet, Broker broker){
        ArrayList<Tranzaction> result = new ArrayList<Tranzaction>();
        try{
        while(resultSet.next()) {
            Instrument instrument = InstrumentDao.findById(resultSet.getInt("instrument_id"));
            Tranzaction Tranzaction = new Tranzaction(
            		 resultSet.getInt("quantity"),
                     resultSet.getDouble("price"),
                     resultSet.getDate("date"),             
                     broker.getId(),
                     instrument.getId(),
                     resultSet.getString("type")
            );
            result.add(Tranzaction);
        }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }

	@Override
	public ArrayList<Tranzaction> getTransactionsByBroker(Broker broker) {
		ArrayList<Tranzaction> tranzaction=new ArrayList<>();
		Statement myStm2;
		try {
			myStm2 = (Statement) connection.createStatement();
			ResultSet rs2=myStm2.executeQuery("select * from transaction where broker_idbroker = "+broker.getId());
			while(rs2.next()){	     
				
				 Tranzaction t = new Tranzaction(
						 rs2.getInt("idtransaction"),
						 rs2.getInt("quantity"),
						 rs2.getDouble("price"),
						 rs2.getDate("date"),             
	                     broker.getId(),
	                     rs2.getInt("instrument_idinstrument"),
	                     rs2.getString("type"));
	                     tranzaction.add(t);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
 
		return tranzaction;
	}

	
	@Override
	public ArrayList<Tranzaction> getTodaysTransactions(String date, Broker broker) {
		ArrayList<Tranzaction> tranzaction=new ArrayList<>();
		Statement myStm2;
		try {
			myStm2 = (Statement) connection.createStatement();
			ResultSet rs2=myStm2.executeQuery("select * from transaction where broker_idbroker = "+broker.getId());
			while(rs2.next()){	     
				if((rs2.getDate("date").toString()).equals(date)){
				 Tranzaction t = new Tranzaction(
						 rs2.getInt("idtransaction"),
						 rs2.getInt("quantity"),
						 rs2.getDouble("price"),
						 rs2.getDate("date"),             
	                     broker.getId(),
	                     rs2.getInt("instrument_idinstrument"),
	                     rs2.getString("type"));
	                     tranzaction.add(t);
			}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
 
		return tranzaction;
	}

	@Override
	public ArrayList<Tranzaction> getIntervalTransactions(String date1, String date2, Broker broker) {
		 ArrayList<Tranzaction> between = new ArrayList<>();
	        double initialFund = 1000000.00;

	        try{
	            statement = (PreparedStatement) connection.prepareStatement(selectBetweenString,Statement.RETURN_GENERATED_KEYS);
	            statement.setString(1,date1);
	            statement.setString(2,date2);
	            statement.setInt(3,broker.getId());
	            resultSet = statement.executeQuery();
	            between = createTranzactionArrayList(resultSet,broker);
	            statement.close();
	        }
	        catch(SQLException e){
	            e.printStackTrace();
	        }
	        return between;
	}



}
