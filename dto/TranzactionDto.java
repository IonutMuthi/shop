package dto;


import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.stream.Collectors;

import org.hibernate.Session;

import connection.ConnectionFactory;
import model.Broker;
import model.Instrument;
import model.Tranzaction;

public class TranzactionDto implements TranzactionInterface {

	 private Session session;

	    public TranzactionDto(){
	    }
	  

	    @Override
	    public Tranzaction findById(int id) {
	        session = (Session) ConnectionFactory.getConnection("Hibernate");
	        Tranzaction Tranzaction = session.get(Tranzaction.class,id);
	        session.close();
	        return Tranzaction;
	    }

	    @Override
	  public  Tranzaction insert(Broker broker, Instrument instrument, Integer quantity, String type){
        session = (Session) ConnectionFactory.getConnection("Hibernate");
	        double amount = quantity * instrument.getPrice();
	        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	        Date d=null;
	        Date date = new Date(d.getTime());
	        Tranzaction Tranzaction = new Tranzaction(
	                quantity,
	                amount,
	               date,
	                instrument.getId(),
	                broker.getId(),
	  	          type
	                
	        );

	        if(type.equalsIgnoreCase("buy")) {
	            double newFund = broker.getFund()-amount;
	            if(newFund<0){
	               System.out.println("Insufficient funds!");
	            }
	            broker.setFund(newFund);
	            session.save(Tranzaction);
	            session.update(broker);
	            session.close();
	        }
	        else {
	            broker.setFund(broker.getFund()+amount);
	            session.update(broker);
	            session.save(Tranzaction);
	            session.close();
	        }
	        return Tranzaction;
	    }

	    @Override
	    public void update(Tranzaction Tranzaction) {
	        session = (Session) ConnectionFactory.getConnection("Hibernate");
	        session.beginTransaction();
	        session.update(Tranzaction);
	        session.getTransaction().commit();
	        session.close();
	    }

	    @Override
	    public void delete(Tranzaction Tranzaction) {
	        session = (Session) ConnectionFactory.getConnection("Hibernate");
	        session.beginTransaction();
	        session.delete(Tranzaction);
	        session.getTransaction().commit();
	        session.close();
	    }



	 

	    @Override
	    public double getFundBeforeDate(String date, Broker broker) {
	        session = (Session) ConnectionFactory.getConnection("Hibernate");
	        String hql = "FROM Tranzaction t inner join t.broker where t.broker.id =:brokerId and t.date <:date";
	        ArrayList<Object[]> result = (ArrayList<Object[]>) session.createQuery(hql)
	                .setParameter("brokerId",broker.getId())
	                .setParameter("date",date)
	                .list();
	        session.close();

	        double initialFund = 1000000.00;
	        for(Object[] t : result){
	            Tranzaction Tranzaction = (Tranzaction) t[0];
	            if(Tranzaction.getType().equalsIgnoreCase("buy")){
	                initialFund = initialFund-Tranzaction.getPrice();
	            }
	            if(Tranzaction.getType().equalsIgnoreCase("sell")){
	                initialFund = initialFund+Tranzaction.getPrice();
	            }
	        }
	        return initialFund;
	    }
	    
		@Override
		public ArrayList<Tranzaction> getTransactionsByBroker(Broker broker) {
			 session = (Session) ConnectionFactory.getConnection("Hibernate");
		        String hql = "FROM Tranzaction t inner join t.broker where t.broker.id =:brokerId";
		        ArrayList<Object[]> result = (ArrayList<Object[]>) session.createQuery(hql).setParameter("brokerId",broker.getId()).list();
		        session.close();
		        ArrayList<Tranzaction> Tranzactions = new ArrayList<Tranzaction>();
		        for(Object[] t : result){
		            Tranzaction Tranzaction = (Tranzaction) t[0];
		            Tranzactions.add(Tranzaction);
		        }

		        return Tranzactions;
		}
		@Override
		public ArrayList<Tranzaction> getTodaysTransactions(String date, Broker broker) {
			ArrayList<Tranzaction> Tranzactions = getTransactionsByBroker(broker);
		        return  Tranzactions.stream().filter(t-> t.getDate().equals(date)).collect(Collectors.toCollection(ArrayList::new));
		   
		}
		@Override
		public ArrayList<Tranzaction> getIntervalTransactions(String date1, String date2, Broker broker) {
			 session = (Session) ConnectionFactory.getConnection("Hibernate");
		        String hql = "FROM Tranzaction t inner join t.broker where t.broker.id =:brokerId and t.date >=:date1 and t.date <=:date2";
		        ArrayList<Object[]> result = (ArrayList<Object[]>) session.createQuery(hql)
		                .setParameter("brokerId",broker.getId())
		                .setParameter("date1",date1)
		                .setParameter("date2",date2)
		                .list();
		        session.close();

		        ArrayList<Tranzaction> Tranzactions = new ArrayList<Tranzaction>();
		        for(Object[] t : result){
		            Tranzaction Tranzaction = (Tranzaction) t[0];
		            Tranzactions.add(Tranzaction);
		        }
		        return Tranzactions;
		}

}
