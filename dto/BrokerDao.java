package dto;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.CallableStatement;
import com.mysql.jdbc.PreparedStatement;

import connection.ConnectionFactory;
import model.Broker;


public class BrokerDao implements BrokerInterface{

	  private static final String selectStatementString = "SELECT * FROM broker WHERE";
	    private static final String insertStatementString = "INSERT INTO broker(username,password) VALUES (?,?)";
	    private static final String loginCheckString = "SELECT * FROM broker where broker.username = ? and broker.password = ?";
	    private static final String updateFundString = "UPDATE broker SET fund = ? WHERE idbroker = ?";
	    private Connection connection;
	    private ResultSet resultSet = null;
	    private PreparedStatement statement = null;
	    private CallableStatement callableStatement = null;

	    public BrokerDao(){
	        connection = (Connection) ConnectionFactory.getConnection("jdbc");
	    }

	    public Broker findById(int id) {
	        Broker result = null;
	        try{
	            statement = (PreparedStatement) connection.prepareStatement(selectStatementString+" idbroker="+id);
	            resultSet = statement.executeQuery();
	            if(resultSet.next()){
	                result = new Broker(resultSet.getString("name"),resultSet.getString("username"),resultSet.getString("password"),resultSet.getFloat("fund"),resultSet.getInt("idbroker"));
	            }
	        }
	        catch(SQLException e){
	            e.printStackTrace();
	        }

	        return result;

	    }

	    public Broker findByUsername(String username) {
	        Broker result = null;
	        try{
	            statement = (PreparedStatement) connection.prepareStatement(selectStatementString+" username= \""+username+"\"");
	            resultSet = statement.executeQuery();
	            if(resultSet.next()){
	                result = new Broker(resultSet.getString("name"),resultSet.getString("username"),resultSet.getString("password"),resultSet.getFloat("fund"),resultSet.getInt("idbroker"));
		            }
	        }
	        catch(SQLException e){
	            e.printStackTrace();
	        }
	        return result;
	    }

	    public Broker register(Broker broker)  {
	        int insertedId;
	        System.out.println("fund = "+broker.getFund());
	     
	            if (isBrokerRegistered(broker.getUsername()) != 0) {
	               System.out.println("Username already exists!");
	            } else {
	                try {
	                    statement = (PreparedStatement) connection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
	                    statement.setString(1, broker.getUsername());
	                    statement.setString(2, broker.getPassword());

	                    statement.executeUpdate();

	                    resultSet = statement.getGeneratedKeys();
	                    if (resultSet.next()) {
	                        insertedId = resultSet.getInt(1);
	                        broker.setId(insertedId);
	                    }
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                }
	            }
	        return broker;
	    }

	    @Override
	    public Broker updateFund(Broker broker, double amount) {
	        double newFund = broker.getFund() - amount;
	        try{
	            statement = (PreparedStatement) connection.prepareStatement(updateFundString);
	            statement.setDouble(1,newFund);
	            statement.setInt(2,broker.getId());
	            statement.executeUpdate();
	            broker.setFund(newFund);
	        }
	        catch(SQLException e){
	            e.printStackTrace();
	        }
	        return broker;
	    }

	    private int isBrokerRegistered(String username) {
	        int exists = 0;
	        try{
	            callableStatement = (CallableStatement) connection.prepareCall("call findBroker(?,?)");
	            callableStatement.setString(1,username);
	            callableStatement.setInt(2,exists);
	            callableStatement.executeUpdate();

	            exists = callableStatement.getInt(2);
	        }
	        catch(SQLException e){
	            e.printStackTrace();
	        }
	        return exists;
	    }

	    public Broker login(String username, String password)  {
	        Broker result = null;

	        try {
	            statement = (PreparedStatement) connection.prepareStatement(loginCheckString, Statement.RETURN_GENERATED_KEYS);
	            statement.setString(1,username);
	            statement.setString(2,password);
	            resultSet = statement.executeQuery();

	            if(resultSet.next()) {
	            	  result = new Broker(resultSet.getString("name"),resultSet.getString("username"),resultSet.getString("password"),resultSet.getFloat("fund"),resultSet.getInt("id"));
			            }
	            if(result == null)
	               System.out.println("Broker is not registered!");
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        return result;
	    }



}
