package dto;

import org.hibernate.Session;

import connection.ConnectionFactory;
import model.Broker;

public class BrokerDto implements BrokerInterface {
	private Session session;
    public BrokerDto(){}
	@Override
	public Broker findById(int id) {
		 session = (Session) ConnectionFactory.getConnection("Hibernate");
	        Broker broker = session.get(Broker.class,id);
	        session.close();
	        return broker;
	}

	@Override
	public Broker findByUsername(String username) {
		   session = (Session) ConnectionFactory.getConnection("Hibernate");
	        String hql = "SELECT broker FROM Broker broker WHERE broker.username =:username";
	        session.close();
	        return (Broker) session.createQuery(hql).setParameter("username",username).uniqueResult();
	    
	}

	@Override
	public Broker login(String username, String password) {
		 session = (Session) ConnectionFactory.getConnection("Hibernate");

	        String hql = "SELECT broker FROM Broker broker WHERE broker.username =:username AND broker.password =:password";
	        Broker broker = (Broker) session.createQuery(hql)
	                               .setParameter("username",username)
	                               .setParameter("password",password)
	                               .uniqueResult();
	        session.close();
	        if(broker == null)
	        {
	        	System.out.println("Broker is not registered!");
	        }
	        	
	        return broker;
			
	}

	@Override
	public Broker register(Broker broker) {
		 session = (Session) ConnectionFactory.getConnection("Hibernate");
	        session.beginTransaction();
	        session.save(broker);
	        session.getTransaction().commit();
	        session.close();
	        broker = findByUsername(broker.getUsername());

	        if(broker == null)
	        {
	        	System.out.println("Broker is not registered!");
	        }
	        return broker;
	}


	@Override
	public Broker updateFund(Broker broker, double amount) {
		// TODO Auto-generated method stub
		return null;
	}

}
