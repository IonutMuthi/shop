package dto;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import connection.ConnectionFactory;
import model.Instrument;

public class InstrumentDao implements InstrumentInterface {

    private static final String selectStatementString = "SELECT * FROM instrument";
    private Connection connection;
    private PreparedStatement statement = null;
    private Statement statement2 = null;
    private ResultSet resultSet = null;

    public InstrumentDao(){
        connection = (Connection) ConnectionFactory.getConnection("jdbc");
    }

    @Override
    public Instrument findById(int idinstrument){
        Instrument result = null;
        try{
            statement = (PreparedStatement) connection.prepareStatement(selectStatementString+" WHERE idinstrument="+idinstrument);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                result = new Instrument(
                        idinstrument,
                        resultSet.getString("name"),
                        resultSet.getFloat("price")
                );
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }

  
    @Override
    public ArrayList<Instrument> findAll() {
       ArrayList<Instrument> result = new ArrayList<Instrument>();
       try{
           statement2 = (Statement) connection.createStatement();
           resultSet = statement2.executeQuery(selectStatementString);
           while(resultSet.next()){
               Instrument instrument = new Instrument(
                       resultSet.getInt("idinstrument"),
                       resultSet.getString("name"),
                       resultSet.getFloat("price")
               );
               result.add(instrument);
           }
       }
       catch(SQLException e){
           e.printStackTrace();
       }
       return result;
    }

    @Override
    public Instrument findByName(String name) {
        Instrument result = null;
        try{
            statement = (PreparedStatement) connection.prepareStatement(selectStatementString+" WHERE name= \""+name+"\"");
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                result = new Instrument(
                        resultSet.getInt("idinstrument"),
                        name,
                        resultSet.getFloat("price")
                );
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return result;
    }


	


}
