package dto;

import java.util.ArrayList;

import model.Instrument;

public interface InstrumentInterface {
	Instrument findById(int id);
    ArrayList<Instrument> findAll();
    Instrument findByName(String name);
}
