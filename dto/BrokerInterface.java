package dto;

import model.Broker;

public interface BrokerInterface {
    Broker findById(int id);
    Broker findByUsername(String username);
    Broker login(String username, String password) ;
    Broker register(Broker broker) ;
    Broker updateFund(Broker broker, double amount);

}
